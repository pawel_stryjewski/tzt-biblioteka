const express = require('express');
const promisify = require('util').promisify;
const mongo = require('mongodb').MongoClient;
const {ObjectId} = require('mongodb');
const assert = require('assert');
var parser = express.urlencoded({extended: true});

var app = express();
var parser = express.urlencoded({extended: true});
var db;

mongo.connect('mongodb+srv://admin:1jRMe2mC2rdNUCjH@cluster0-uapmc.mongodb.net/test?retryWrites=true&w=majority', function(err, client) {
    assert.equal(null, err);
    db = client.db('cinema');
    console.log("Nawiązano połączenie z bazą");
    seed((err) => {
        console.log(err);
    });
});

app.get('/', async (req, res) => {
    if(req.session.userId != null) {
        var user = await promisify(getUserData)(req.session.userId);
        res.send(`
        <h1>Kino</h1><br />
        
        `);
    } else {
        res.send(`
        <h1><a href="/timetable">Repertuar</a></h1>
        <h1><a href="/movies">Filmy</a></h1>
        `);
    }
});

app.get('/timetable', async(req, res) => {
    try {
        res.send(`
        <form method="post" action="timetable">
            Zakres dat:<br />
            Od:<br />
            <input type="date" value="` + formatDate(new Date()) + `" name="startDate" /><br />
            Do:<br />
            <input type="date" value="` + formatDate(new Date()) + `" name="endDate" /><br />
            <br />
            <input type="submit" value="Dalej" />
        </form>
        `);
    } catch(err) {
        res.send('Wystąpił błąd');
    }
});

app.post('/timetable', parser, async(req, res) => {
    try {
        const timetable = await promisify(getTimeTable)(req.body.startDate, req.body.endDate);
        const mappedTimetable = timetable.map(show => `<tr><td>` + show.movie.title + `</td><td>` + formatDateTime(show.time) +
        `<td><form action="/orderTicket" method="get">
            <input type="hidden" name="showId" value="`+ show._id +`" />
            <button type="submit">Bilet</button>
        </form></td>
        </tr>`).reduce((accumulator, currentValue) => accumulator += currentValue, "");
        res.send(`
            <h1>Repertuar:</h1><br /><table border=1><td>Tytuł</td><td>Data i godzina emisji</td>` + mappedTimetable + `</table>`)
    } catch(err) {
        res.send('Wystąpił błąd');
    }
});

app.get('/orderTicket', async(req, res) => {
    try {
        const types = await promisify(getTicketTypes)();
        const ticketOptions = types.map(element => `<option value="${element._id}">${element.name} - ${element.price} zł`)
        .reduce((accumulator, currentValue) => accumulator += currentValue, "");
        res.send(`
        <form method="post" action="orderTicket">
            Dane biletu:<br />
            Imię:<br />
            <input type="text" name="firstName" /><br />
            Nazwisko:<br />
            <input type="text" name="lastName" /><br />
            Typ biletu:<br />
            <select name="tickettype">${ticketOptions}</select>
            <br />
            <input type="hidden" name="showId" value="`+ req.query.showId +`" />
            <input type="submit" value="Kontynuuj" />
        </form>
        `);
    } catch(err) {
        res.send('Wystąpił błąd');
    }
});

app.post('/orderTicket', parser, async(req, res) => {
    try {
        await promisify(orderTicket)(req.body.showId, req.body.tickettype, req.body.firstName, req.body.lastName);
        res.send(`Zapisano<br /><a href="/timetable">OK</a>`);
    } catch(err) {
        res.send('Wystąpił błąd');
    }
});

app.get('/movies', async(req, res) => {
    try {
        const movies = await promisify(getPlayedMovies)();
        const mappedMovies = movies.map((movie, index) => `<tr><td>` + (index + 1) + `</td><td>` + movie.title + `</td><td>` + movie.genre.name +
        `<td><form action="/movieDetails" method="get">
            <input type="hidden" name="showId" value="`+ movie._id +`" />
            <button type="submit">Szczegóły</button>
        </form></td>
        </tr>`).reduce((accumulator, currentValue) => accumulator += currentValue, "");
        res.send(`
            <h1>Aktualnie grane filmy:</h1><br /><table border=1>` + mappedMovies + `</table>`)
    } catch(err) {
        res.send('Wystąpił błąd');
    }

});

app.get('/movieDetails', async(req, res) => {
    try {
        const movieDetails = await promisify(getMovieDetails)(movieId);
        res.send(`
            <h1>` + movieDetails.title + `</h1><br />Data premiery: ` + formatDate(movieDetails.releaseDate) + `    Gatunek: ` + movieDetails.genre.name + `<br />`+ movieDetails.description + `
        `)
    } catch(err) {
        res.send('Wystąpił błąd');
    }
});

async function getPlayedMovies(callback) {
    try {
        const movies = await db.collection('movies').find({isPlayed: true}).toArray();
        callback(null, movies);
    } catch(err) {
        callback(err, null);
    }
}

async function getMovieDetails(movieId, callback) {
    try {
        const movie = await db.collection('movies').findOne({_id: ObjectId(movieId)});
        callback(null, movies);
    } catch(err) {
        callback(err, null)
    }
}

async function getTimeTable(dateStart, dateEnd, callback) {
    try {
        const shows = await db.collection('shows').find({time: {$gte: new Date(dateStart), $lt: new Date(dateEnd)}}, {projection: {tickets: 0}}).toArray();
        console.log(shows);
        callback(null, shows);
    } catch(err) {
        callback(err, null);
    }
}

async function getFreeSeats(showId, callback) {
    try {
        const show = await db.collection('shows').findOne({_id: ObjectId(showId)});
        const room = await db.collection('rooms').findOne({_id: ObjectId(show.room)});

        const boughtCount = show.tickets.length;
        const freeSeats = room.seats - boughtCount;
        callback(null, freeSeats);
    } catch(err) {
        callback(err, null);
    }
}

async function getTicketTypes(callback) {
    try {
        const types = await db.collection('tickettypes').find().toArray();
        console.log(types);
        callback(null, types);
    } catch(err) {
        callback(err, null);
    }
}

async function orderTicket(showId, ticketTypeId, firstName, lastName, callback) {
    try {
        const ticket = await db.collection('tickettypes').findOne({_id: ObjectId(ticketTypeId)});
        const show = await db.collection('shows').findOne({_id: ObjectId(showId)});
        const room = await db.collection('rooms').findOne({_id: ObjectId(show.room)});

        const freeSeats = await promisify(getFreeSeats)(showId);
        if(freeSeats > 0) {
            await db.collection('orders').insertOne({firstName: firstName, lastName: lastName, price: ticket.price, movie: show.movie});
            await db.collection('shows').updateOne(
                { _id: ObjectId(showId)}, 
                {$push: 
                    {"tickets": 
                        { firstName: firstName, lastName: lastName, price: ticket.price}
                    }
                }
            );
            const ticketVm = { firstName: firstName, lastName: lastName, price: ticket.price, room: room.name };
            callback(null, ticketVm);
        } else {
            callback(new SeatsError("No free seats"), null);
        }
    } catch(err) {
        callback(err);
    }
}

async function seed(callback) {
    try {
        const genres = [
            {_id: ObjectId(), name: "Komedia"},
            {_id: ObjectId(), name: "Przygodowy"},
            {_id: ObjectId(), name: "Akcja"},
            {_id: ObjectId(), name: "Dramat"},
        ]
        if((await db.collection('genres').find().toArray()).length == 0) {
            db.collection('genres').insertMany(genres);
        }

        const movies = [
            {_id: ObjectId(), title: "Futro z Misia", genre: genres[0], release: new Date('2019-12-27'), isPlayed: true, description: "Boss podhalańskiej mafii, o wiele mówiącej ksywie Nerwowy, jest wściekły. To akurat w jego przypadku normalne, bo facet z natury nie jest oazą spokoju, ale tym razem ma naprawdę dobry powód. "},
            {_id: ObjectId(), title: "Gwiezdne Wojny: Skywalker. Odrodzenie", genre: genres[1], release: new Date('2019-12-20'), isPlayed: true, description: "Członkowie organizacji Opór ponownie stawiają czoła Najwyższemu Porządkowi."},
            {_id: ObjectId(), title: "Jak zostałem gangsterem. Historia prawdziwa", genre: genres[2], release: new Date('2020-01-03'), isPlayed: true, description: "„Jak zostalem gangsterem. Historia prawdziwa” to film o przyjazni i milosci w czasach, w których pozwolenie sobie na emocje moze okazac sie tragiczne w skutkach."},
            {_id: ObjectId(), title: "Judy", genre: genres[3], release: new Date('2020-01-03'), isPlayed: true, description: "Judy Garland, cudowne dziecko sceny i wielkiego ekranu, dziewczyna, która podbiła serca światowej publiczności dzięki niewinnej urodzie, niezwykłemu głosowi i szczególnej charyzmie. "},
            {_id: ObjectId(), title: "Jumanji", genre: genres[1], release: new Date('2019-12-27'), isPlayed: true, description: "„Jumanji: Następny poziom” to powrót słynnych bohaterów, których poznaliśmy w „Jumanji: Przygoda w dżungli”. "},

        ]
        if((await db.collection('movies').find().toArray()).length == 0) {
            db.collection('movies').insertMany(movies);
        }

        const rooms = [
            {_id: ObjectId(), name: "1", seats: 50},
            {_id: ObjectId(), name: "2", seats: 40},
            {_id: ObjectId(), name: "3", seats: 70},
            {_id: ObjectId(), name: "4", seats: 90},
            {_id: ObjectId(), name: "5", seats: 30},
            {_id: ObjectId(), name: "6", seats: 45}
        ]
        if((await db.collection('rooms').find().toArray()).length == 0) {
            db.collection('rooms').insertMany(rooms);
        }

        const ticketTypes = [
            {_id: ObjectId(), name: "Normalny", price: 25},
            {_id: ObjectId(), name: "Ulgowy", price: 15},
            {_id: ObjectId(), name: "Studencki", price: 18},
        ]
        if((await db.collection('tickettypes').find().toArray()).length == 0) {
            db.collection('tickettypes').insertMany(ticketTypes);
        }

        const shows = [
           {_id: ObjectId(), movie: movies[0], time: Date(2020,01,02,16,00,00,00), room: rooms[0]._id, tickets: [ {firstName: "John", lastName: "Doe", price: 25}, {firstName: "Jane", lastName: "Doe", price: 25} ]},
           {_id: ObjectId(), movie: movies[1], time: Date(2020,01,03,29,00,00,00), room: rooms[1]._id, tickets: [ {firstName: "John", lastName: "Doe", price: 25}, {firstName: "Jane", lastName: "Doe", price: 25} ]}
        ]
        if((await db.collection('shows').find().toArray()).length == 0) {
            db.collection('shows').insertMany(shows);
        }

        const orders = [
            {id: ObjectId(), firstName: "John", lastName: "Doe", price: 25, movie: movies[0]._id},
            {id: ObjectId(), firstName: "Jane", lastName: "Doe", price: 25, movie: movies[0]._id},
            {id: ObjectId(), firstName: "John", lastName: "Doe", price: 25, movie: movies[1]._id},
            {id: ObjectId(), firstName: "Jane", lastName: "Doe", price: 25, movie: movies[1]._id}
        ]
        if((await db.collection('orders').find().toArray()).length == 0) {
            db.collection('orders').insertMany(orders);
        }

        callback(null);
    } catch(err) {
        callback(err);
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

function formatDateTime(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
        hour = '' + d.getHours();
        minute = '' + d.getMinutes();
    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    if (minute.length < 2)
        minute = '0' + minute;

    return [year, month, day].join('-') + ' ' + [hour, minute].join(':');
}

app.listen(3000, function () {
    console.log('Library app listening on port 3000!');
  });

class SeatsError extends Error {
constructor(message) {
    super(message);
    this.name = "SeatsError";
}
}
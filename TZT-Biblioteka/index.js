var express = require('express');
var Redis = require('ioredis');
const promisify = require('util').promisify;

var app = express();
var parser = express.urlencoded({extended: true});
var redis = new Redis({
    port: 11053,
    host: "redis-11053.c135.eu-central-1-1.ec2.cloud.redislabs.com",
    password: '5zy2XcW081sxmWmhAWCV2HEyYCpJkcqQ',
    db: 0
});

app.get('/', async (req, res) => {
    const name = await promisify(redis.get).bind(redis)('libraryname');
    res.send(`<h1>${name}</h1><br />
    <a href="/borrowbook">Wypozycz ksiazke</a><br />
    <a href="/returnbook">Zwroc ksiazke</a><br />
    <a href="/addperson">Dodaj osobe</a><br />
    <a href="/addbook">Dodaj ksiazke</a><br />
    `);
});

app.get('/addperson', function (req, res) {  
    res.send(`
    <script> function goBack() { window.history.back(); } </script>
    Dodaj osobę:<br>
    <form method="post" action="addperson">
      Imię:<br />
      <input type="text" name="name" /><br />
      Nazwisko:<br />
      <input type="text" name="surname" /><br />
      <br />
      <input type="submit" value="Dodaj" />
    </form>
    <button onclick="goBack()">Wróc</button>
    `)
})

app.get('/addbook', function (req, res) {  
    res.send(`
    <script> function goBack() { window.history.back(); } </script>
    Dodaj ksiąkę:<br>
    <form method="post" action="addbook">
      Tytuł:<br />
      <input type="text" name="title" /><br />
      Autor:<br />
      <input type="text" name="author" /><br />
      Wydawnictwo:<br />
      <input type="text" name="publish" /><br />
      <br />
      <input type="submit" value="Dodaj" />
    </form>
    <button onclick="goBack()">Wróc</button>
    `)
})

app.post('/addperson', parser, async (req, res) => {
    try {
        var id = await promisify(redis.incr).bind(redis)('people');
        redis.hmset('people:' + id, ["name", req.body.name, "surname", req.body.surname]);
        res.redirect('/');  
    } catch(error) {
        console.log(err)
        res.sendStatus(500);
    }
})

app.post('/addbook', parser, async(req, res) => {
    console.log(req.body);
    try {
        var id = await promisify(redis.incr).bind(redis)('people');
        redis.hmset('books:' + id, ["title", req.body.title, "author", req.body.author, "publisher", req.body.publisher]);
        res.redirect('/');  
    } catch(error) {
        console.log(err)
        res.sendStatus(500);
    }
})

app.get('/allbooks', async(req, res) => {
    try {
        const books = await promisify(getAllBooks)().catch((err) => console.log(err));
        const mappedBooks = books.map(book => book.title + " - " + book.author + "<br>").reduce((accumulator, currentValue) => accumulator += currentValue);
        res.send(mappedBooks);
    } catch(error) {
        console.log(err)
        res.sendStatus(500);
    }
})

app.get('/allpeople', async(req, res) => {
    try {
        const people = await promisify(getAllPeople)().catch((err) => console.log(err));
        const mappedPeople = people.map(person => person.name + " " + person.surname + "<br>").reduce((accumulator, currentValue) => accumulator += currentValue);
        res.send(mappedPeople);
    } catch(error) {
        console.log(err)
        res.sendStatus(500);
    }    
})

app.get('/borrowbook', async(req, res) => {  
    try {
        const people = await promisify(getAllPeople)().catch((err) => console.log(err));
        var peopleOptions = people.reduce((accumulator, currentValue) => 
            accumulator += "<option value='" + currentValue.id + "'>" + currentValue.name + " " + currentValue.surname + "</option>", "")

        const books = await promisify(getAvailableBooks)().catch((err) => console.log(err));
        var booksOptions = books.reduce((accumulator, currentValue) => 
            accumulator += "<option value='" + currentValue.id + "'>" + currentValue.title + " - " + currentValue.author + "</option>", "")

        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Wypozycz ksiąkę:<br>
        <form method="post" action="borrowbook">
            Ksiązka:<br />
            <select name="book">` +
            booksOptions +
            `</select><br />
            Osoba:<br />
            <select name="person">` +
            peopleOptions +
            `</select>
            <br />
            <input type="submit" value="Wypozycz" />
        </form>
        <button onclick="goBack()">Wróc</button>
        `)   
    } catch(err) {
        console.log(err)
        res.sendStatus(500);
    }
})

app.post('/borrowbook', parser, async(req, res) => {
    try {
        var id = await promisify(redis.incr).bind(redis)('borrowed');
        redis.hmset('borrowed:' + id, ["book", req.body.book, "person", req.body.person]);
        res.redirect('/');   
    } catch(err) {
        console.log(err)
        res.sendStatus(500);
    }
})

app.get('/returnbook', async(req, res) => {  
    try {
        const books = await promisify(getBorrowedBooks)().catch((err) => console.log(err));
        var booksOptions = books.reduce((accumulator, currentValue) => 
            accumulator += "<option value='" + currentValue.id + "'>" + currentValue.title + " - " + currentValue.author + "</option>", "")

        res.send(`
        <script> function goBack() { window.history.back(); } </script>
        Zwróc ksiąkę:<br />
        <form method="post" action="returnbook">
            Ksiązka:<br />
            <select name="book">` +
            booksOptions +
            `</select><br />
            <input type="submit" value="Zwroc" />
        </form>
        <button onclick="goBack()">Wróc</button>
        `)   
    } catch(err) {
        console.log(err)
        res.sendStatus(500);
    }
})

app.post('/returnbook', parser, async(req, res) => {
    try {
        const borrowedBooks = await promisify(getBorrowedBooksKeys)().catch((err) => console.log(err));
        const borrowElement = borrowedBooks.find((element) => {
            return element.book == req.body.book
        })
        console.log(borrowElement);
        redis.del(borrowElement.id);
        res.redirect('/');   
    } catch(err) {
        console.log(err)
        res.sendStatus(500);
    }
})

function getAllPeople(callback) {
    try {
        var pipeline = redis.pipeline();
        redis.keys('people:*', function(err, klucze) {
            for (var i = 0; i < klucze.length; i++) {
                pipeline.hgetall(klucze[i]);
            }
            pipeline.exec(function(err,result){
                var people = [];
                for (var i = 0; i < klucze.length; i++) {
                    people.push({ id: klucze[i], name: result[i][1].name, surname: result[i][1].surname});
                }
                callback(null, people);
            });
        });    
    } catch(err) {
        callback(err, null);
    }
}

function getAllBooks(callback) {
    try {
        var pipeline = redis.pipeline();
        redis.keys('books:*', function(err, klucze) {
            for (var i = 0; i < klucze.length; i++) {
                pipeline.hgetall(klucze[i]);
            }
            pipeline.exec(function(err,result){
                var books = [];
                for (var i = 0; i < klucze.length; i++) {
                    books.push({ id: klucze[i], title: result[i][1].title, author: result[i][1].author, publisher: result[i][1].publisher});
                }
                callback(null, books);
            });
        });    
    } catch(err) {
        callback(err, null);
    }
}

function getBorrowedBooksKeys(callback) {
    try {
        var pipeline = redis.pipeline();
        redis.keys('borrowed:*', function(err, klucze) {
            for (var i = 0; i < klucze.length; i++) {
                pipeline.hgetall(klucze[i]);
            }
            pipeline.exec(function(err,result){
                var books = [];
                for (var i = 0; i < klucze.length; i++) {
                    books.push({ id: klucze[i], book: result[i][1].book, person: result[i][1].person});
                }
                callback(null, books);
            });
        });   
    } catch(err) {
        callback(err, null);
    }
}

async function getBorrowedBooks(callback) {
    try {
        const allBooks = await promisify(getAllBooks)().catch((err) => console.log(err));
        const borrowedBooksKeys = await promisify(getBorrowedBooksKeys)().catch((err) => console.log(err));
        const borrowedBooks = allBooks.filter((element) => {
            return borrowedBooksKeys.some((borrowed) => { 
                return borrowed.book.toString() === element.id.toString();
            })
        })
        callback(null, borrowedBooks); 
    } catch(err) {
        callback(err, null)
    }
}

async function getAvailableBooks(callback) {
    try {
        const allBooks = await promisify(getAllBooks)().catch((err) => console.log(err));
        const borrowedBooks = await promisify(getBorrowedBooksKeys)().catch((err) => console.log(err));
        const availableBooks = allBooks.filter((element) => {
            return !borrowedBooks.some((borrowed) => { 
                return borrowed.book.toString() === element.id.toString();
            })
        })
        callback(null, availableBooks); 
    } catch(err) {
        callback(err, null);
    }
}



app.listen(3000, function () {
    console.log('Library app listening on port 3000!');
  });